/*
  Read Analog input and send it via serial port.
  Adapted from AnalogInOutSerial built-in example
*/

const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to

int sensorValue = 0;        // value read from the pot
//float voltage = 0.0;

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
}

void loop() {
  // read the analog in value:
  sensorValue = analogRead(analogInPin);
  //voltage = sensorValue * (5.0/ 1023.0);

  // print the results to the Serial port
  Serial.println(sensorValue);

  delay(10);
}
