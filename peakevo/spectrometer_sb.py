#  Copyright 2017 Carlos Pascual-Izarra <cpascual@users.sourceforge.net>
#
#  This file is part of PeakEvo.
#
#  PeakEvo is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  PeakEvo is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.


import seabreeze
seabreeze.use('pyseabreeze')
from seabreeze.spectrometers import Spectrometer, list_devices
import numpy

class SBSpectrometer(Spectrometer):
    """A SeaBreeze Spectrometer class with some API candy"""

    def __init__(self, dev=None, integ_time=None):
        if dev is None:
            dev = list_devices()[0]
        Spectrometer.__init__(self, dev)
        if integ_time is None:
            integ_time = self.minimum_integration_time_micros
        self._integ_time = integ_time
        self.integration_time_micros(integ_time)

    @property
    def integration_time(self):
        return self._integ_time

    @integration_time.setter
    def integration_time(self, integ_time):
        self.integration_time_micros(integ_time)
        self._integ_time = integ_time

    def capture(self):
        return (numpy.asarray(self.wavelengths(), dtype='float64'),
                numpy.asarray(self.intensities(), dtype='float64'))


if __name__ == "__main__":
    def _test1():
        s = SBSpectrometer()
        for i in range(3):
            print(s.capture(), s.integration_time)

    _test1()
