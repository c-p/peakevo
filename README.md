# PeakEvo

An application to acquire data from a spectrometer (supports OceanView hardware
via the seabreeze module) and track the position of a peak over time.

## Install

```
python setup.py install
```

## Launch

```
peakevo
```

## Requirements

- Python
- qtpy (+ PyQt5 **or** PyQt4 **or** PySide)
- PyQtGraph
- python-seabreeze (tested with pyseabreeze backend)